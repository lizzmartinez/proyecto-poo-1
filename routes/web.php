<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=> ['auth']], function(){
    
   
    Route::get('/lista/productos', ['as'=>'list.productos','uses'=>'ProductoController@InicioProducto']);
    Route::get('/crear/productos', ['as'=>'crear.productos','uses'=>'ProductoController@CrearProducto']);
    Route::post('/guardar/productos', ['as'=>'guardar.productos','uses'=>'ProductoController@GuardarProducto']);

    //rutas cliente
    Route::get('/lista/cliente', ['as'=>'list.cliente','uses'=>'ClienteController@InicioCliente']);
    Route::get('/crear/cliente', ['as'=>'crear.cliente','uses'=>'ClienteController@CrearCliente']);
    Route::post('/guardar/cliente', ['as'=>'guardar.cliente','uses'=>'ClienteController@GuardarCliente']);

});