<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;


class ClienteController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function InicioCliente(Request $request)
  {
    //dd('holam');

    $cliente = Cliente::all();


    return view('cliente.inicio')->with('cliente', $cliente);
  }
  public function CrearCliente(Request $request)
  {
    $cliente = Cliente::all();
    return view('cliente.crear')->with('cliente', $cliente);
  }
  public function  GuardarCliente(Request $request)
  {
    $this->validate($request, [
      'nombre' => 'required', 
      'apellidos' => 'required',
      'cedula' => 'required',
      'direccion' => 'required',
      'telefono' => 'required',
      'fecha_nacimiento' => 'required',
      'email' => 'required'
    ]);

    $cliente = new Cliente;
    $cliente->nombre                  = $request->nombre;
    $cliente->apellidos               = $request->apellidos;
    $cliente->cedula                  = $request->cedula;
    $cliente->direccion               = $request->direccion;
    $cliente->telefono                = $request->telefono;
    $cliente->fecha_nacimiento        = $request->fecha_nacimiento;
    $cliente->email                   = $request->email;
    $cliente->save();
    return redirect()->route('list.cliente');
  }
  
}
