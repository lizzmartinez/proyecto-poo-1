@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR CLIENTE</div>
                <div class="col text-right">
                    <a href="{{route('list.cliente')}}" class="btn btn-sm btn-success">Cancelar</a>
                </div>
                <div class="card-body">
                    <form role="form" method="POST" action="{{route('guardar.cliente')}}">
                        {{ csrf_field() }}
                        {{ method_field('POST')}}
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="nombre">Nombre del cliente</label>
                                <input type="text" class="from-control" name="nombre">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="apellidos">Apellido del cliente</label>
                                <input type="text" class="from-control" name="apellidos">
                            </div>

                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="cedula">Cedula del cliente</label>
                                <input type="text" class="from-control" name="cedula">
                            </div>
                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="direccion">Direccion del cliente</label>
                                <input type="text" class="from-control" name="direccion">
                            </div>
                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="telefono">Telefono del cliente</label>
                                <input type="text" class="from-control" name="telefono">
                            </div>
                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="fecha_nacimiento">Fecha nacimiento del cliente</label>
                                <input type="date" class="from-control" name="fecha_nacimiento">
                            </div>
                            <div class="col-lg-4">
                                <label class="from-control-label mr-2" for="email">Email del cliente</label>
                                <input type="text" class="from-control" name="email">
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success pull-right"> Guardar </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection